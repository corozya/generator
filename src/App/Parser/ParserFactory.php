<?php

namespace App\Parser;

class ParserFactory {

    static function getParser(\Symfony\Component\HttpFoundation\File\UploadedFile $file) {


        switch (strtolower($file->getClientOriginalExtension())) {
            case 'xls':
            case 'xlsx':
                $inputFileName = $file->getPathname();
                $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
                $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);
                return array_slice($sheetData, 1);
                break;
            case 'csv':
                $handle = fopen($file->getPathname(), 'r');
                $i = 0;
                $out=[];
                while (($data = fgetcsv($handle, null, ';') ) !== FALSE) {
                    if ($i > 0) {
                            $out[]=$data;
                    }
                    $i++;
                }
                return $out;
                break;
        }
    }

}
