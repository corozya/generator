<?php

namespace App\Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use App\Parser\ParserFactory as ParserFactory;

class GeneratorController implements ControllerProviderInterface {

    protected $app;

    public function connect(Application $app) {
        $this->app = $app;
        $controllers = $app['controllers_factory'];
        $controllers->get('/', array($this, 'indexAction'))->bind('generator');
        $controllers->post('/', array($this, 'indexAction'))->bind('generator_post');
        return $controllers;
    }

    protected function getForm() {
        
        
        $form = $this->app['form.factory']->createBuilder(FormType::class)
                ->add('filename', FileType::class, array('label' => false, 'attr' => array('style' => 'display:none')))
                ->add('watermark', CheckboxType::class,
                    array(
                        'label' => 'Watermark',
                        'attr' => array(),
                        'label_attr' => array(
                                'class' => 'watermark_label'
                        )
                    )
                )
                ->getForm();
        return $form;
    }

    public function indexAction(Application $app, Request $request) {
        
        $form = $this->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $generator = \App\Generator\GeneratorFactory::getGenerator($this->app['generator']);
            $zip = new \ZipArchive();
            
            if (@file_exists('test.zip')) {
                $res = $zip->open('test.zip', \ZipArchive::OVERWRITE);
            } else {
                $res = $zip->open('test.zip', \ZipArchive::CREATE);
            }
            if ($res === TRUE) {
                $file = $form['filename']->getData();
                $data = ParserFactory::getParser($file);
                $i = 0;
                foreach ($data as $item) {
                    $image = new \App\Image();
                    $image->setTitle($item[0]);
                    $image->setAuthor($item[1]);
                    $image->setContent($item[2]);
                    $image->setBackground($item[3]);
                    if (@$item[4]) {
                        $image->setFont($item[4]);
                    }
                    if (@$item[5]) {
                        $image->setColor($item[5]);
                    }
                    $image->setWatermark($form['watermark']->getData());

                    $jpg = $generator->process($image);
                    //$jpg->show();
                    //echo '<img src="data:image/jpeg;base64,'.base64_encode($jpg->getImageContent()).'">';
                    $zip->addFromString($i . '.png', $jpg);
                    $i++;
                    
                }
                
                $zip->close();
                $response = new Response;
                $response->setStatusCode(200);
                $response->headers->set('Content-Type', 'application/xml');
                $response->headers->set('Content-Description', 'Submissions Export');
                $response->headers->set('Content-Disposition', 'attachment; filename=cytaty.zip');
                $response->headers->set('Content-Transfer-Encoding', 'binary');
                $response->headers->set('Pragma', 'no-cache');
                $response->headers->set('Expires', '0');
                $response->setContent(file_get_contents('test.zip'));
                
                return $response;
            }
        }

        return $app['twig']->render('generator.html.twig', array('form' => $form->createView()));
    }

}
