<?php

namespace App;

class Image {

    public $content;
    public $background;
    public $title;
    public $author;
    public $font;
    public $color;
    public $watermark;
    protected $rendered = null;

    public function __construct($content=null, $background = '1.jpg', $title = null, $author = null, $font = 'Lato-Regular.ttf', $color=null) {
        $this->content = $content;
        $this->background = $background;
        $this->title = $title;
        $this->author = $author;
        $this->font = $font;
        $this->color = $color;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content=$content;
    }

    public function getBackground() {
        return $this->background;
    }

    public function setBackground($background) {
        $this->background=$background;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title=$title;
    }

    public function getAuthor() {
        return $this->author;
    }

    public function setAuthor($author) {
        $this->author=$author;
    }

    public function getFont() {
        return $this->font;
    }

    public function setFont($font) {
        $this->font=$font;
    }

    public function getColor() {
        return $this->color;
    }

    public function setColor($color) {
        $this->color=$color;
    }
    public function getWatermark() {
        return $this->watermark;
    }

    public function setWatermark($watermark) {
        $this->watermark=$watermark;
    }

    public function setRendered($image) {
        $this->rendered = $image;
    }

    public function getImage() {
        return imagejpg($this->rendered);
    }

    public function getImageContent() {
        ob_start();
        imagejpeg($this->rendered);
        $rawImageBytes = ob_get_clean();
        return $rawImageBytes;
    }

    public function show() {
        header('Content-Type: image/jpeg');

        $this->getImage();
    }

}
