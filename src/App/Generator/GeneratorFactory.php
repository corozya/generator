<?php

namespace App\Generator;

class GeneratorFactory {

    static function getGenerator($type) {


        switch (strtolower($type)) {
            case 'imagick':
                return new \App\Generator\ImagickGenerator();
                break;
            case 'gd':
                return new \App\Generator\GdGenerator();
                break;
        }
    }

}
