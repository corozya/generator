<?php

namespace App\Generator;

class ImagickGenerator extends AbstractGenerator{

    protected $backgrounds;

    public function __construct() {
        $file = __DIR__ . '/../../../config/background.xml';
        $xml = simplexml_load_file($file);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);
        $this->backgrounds = $array['background'];
    }

    private function _getFontColor($image) {
        if ($image->getColor() != '') {
            $font_color = $image->getColor();
        } else {
            foreach ($this->backgrounds as $item) {
                if (strtolower($item['filename']) == strtolower($image->getBackground())) {
                    if ($item['textcolor']) {
                        $font_color = $item['textcolor'];
                    }
                    break;
                }
            }
        }
        return $font_color;
    }

    public function process($image) {

        $handle = fopen('backgrounds/' . $image->getBackground(), 'rb');

        if($image->getWatermark()) {
            $watermark = new \Imagick();
            $watermark->readImage('logo/postLogo.png');
        }
        $draw = new \ImagickDraw();

        $img = new \Imagick();
        $img->readImageFile($handle);
        $img->resizeImage(940, 940, 0, 0);

        if($watermark) {
            $img->compositeImage($watermark, \Imagick::COMPOSITE_OVER, 770, 890);
        }
        $draw->setFillColor($this->_getFontColor($image));
        $draw->setFont('fonts/' . $image->getFont());
        
        $fontSize=30;
        $draw->setFontSize($fontSize);

        /* Create text */
        $content = $image->getContent();
        $title = $image->getTitle();
        $author = $image->getAuthor();
        
        if (strlen($content) < 150) {
            $body = wordwrap($content, 36, PHP_EOL);
        } elseif (strlen($content) > 500) {
            $body = wordwrap($content, 70, PHP_EOL);
        } else {
            $body = wordwrap($content, 55, PHP_EOL);
        }
        
        
        $body.=PHP_EOL . PHP_EOL . '- ' . $author;
        $body.=PHP_EOL . $title;
        $textBlockSize=$img->queryFontMetrics($draw, $body, true);

        
        $scale = 860 / $textBlockSize['textWidth'];
        $fontSize=$fontSize* $scale;
        $draw->setFontSize(min($fontSize, 40));
        //var_dump($draw->getTextInterlineSpacing() );
         $draw->setTextInterlineSpacing(5);
        
        $textBlockSize=$img->queryFontMetrics($draw, $body, true);

       
        
        $textPosY=((940/2)-($textBlockSize['textHeight'])/2) +(2 * $textBlockSize['ascender']);
//        var_dump($textPosY);
//        die();
        //$img->resizeImage($textBlockSize['textWidth'], $textBlockSize['textHeight'], 0, 0);
        //$img->annotateImage($draw, 0, $textBlockSize['ascender'], 0, $body);
        $img->annotateImage($draw, 40, $textPosY, 0, $body.$textBlockSize['characterWidth']);

        $img->setImageFormat('jpg');

        return $img;
        header('Content-type: image/png');
        echo $img;
        die();

        $magRows = 23;

        $logo = @imagecreatefrompng('logo/postLogo.png');
        $background = @imagecreatefromjpeg('backgrounds/' . $image->getBackground());
        $destination_image = imagecreatetruecolor(imagesx($background), imagesy($background));

        if ($image->getColor() != '') {
            $rgb = $this->hex2rgb($image->getColor());
            $font_color = imagecolorallocate($destination_image, $rgb[0], $rgb[1], $rgb[2]);
        } else {
            $font_color = imagecolorallocate($destination_image, 128, 128, 128);

            foreach ($this->backgrounds as $item) {
                if (strtolower($item['filename']) == strtolower($image->getBackground())) {
                    if ($item['textcolor']) {
                        $rgb = $this->hex2rgb($item['textcolor']);
                        $font_color = imagecolorallocate($destination_image, $rgb[0], $rgb[1], $rgb[2]);
                    }
                    break;
                }
            }
        }
        $font = 'fonts/' . $image->getFont();
        $content = $image->getContent();
        $title = $image->getTitle();
        $author = $image->getAuthor();



        imagecopy($destination_image, $background, 0, 0, 0, 0, 940, 940);
        imagecopy($destination_image, $logo, 770, 890, 0, 0, imagesx($logo), imagesy($logo));


        imagealphablending($destination_image, true);
        imagesavealpha($destination_image, true);
        if (strlen($content) < 150) {
            $body = wordwrap($content, 36, PHP_EOL);
        } elseif (strlen($content) > 500) {
            $body = wordwrap($content, 70, PHP_EOL);
        } else {
            $body = wordwrap($content, 55, PHP_EOL);
        }
//        var_dump(substr_count($body, PHP_EOL));
//        
//        if(substr_count($body, PHP_EOL)){
//            $body= wordwrap($content, 60, PHP_EOL);
//            var_dump(substr_count($body, PHP_EOL));
//        }

        $body.=PHP_EOL . PHP_EOL . '- ' . $author;
        $body.=PHP_EOL . $title;

        $fontsizeinPT = 40;
        $spacing = 1.4;
        $box = imageftbbox($fontsizeinPT, 0, $font, $body);
        $scale = 860 / ($box[4] - $box[0]);

        $fontsizeinPT = $fontsizeinPT * $scale;

        do {
            $fontsizeinPT = $fontsizeinPT - 1;
            $box = imageftbbox($fontsizeinPT, 0, $font, $body);
            $tw = $box[4] - $box[0]; //image width
            $th = ($box[1] - $box[5]) * $spacing;
        } while ($th > 860);
//
//        $fontsizeinPT = $fontsizeinPT - 1;
        $fontsizeinPT = min($fontsizeinPT, 40);
        $box = imageftbbox($fontsizeinPT, 0, $font, $body);
        $tw = $box[4] - $box[0]; //image width
        $th = ($box[1] - $box[5]) * $spacing; //image height
        $lines = explode(PHP_EOL, $body);

        //line height
        $lineBox = imageftbbox($fontsizeinPT, 0, $font, $lines[0]);
        $lineh = ($lineBox[1] - $lineBox[5]);
        $lineSpacing = $lineh * $spacing;

        $y = 470 - ((count($lines) - 2) * $lineSpacing / 2);
        foreach ($lines as $line) {
            imagettftext($destination_image, $fontsizeinPT, 0, 40, $y, $font_color, $font, $line);
            $y+=$lineSpacing;
        }
        $image->setRendered($destination_image);
        return $image;
    }

}
