<?php

namespace App\Generator;

class GdGenerator extends AbstractGenerator {

    public function process($image) {

        $magRows = 23;
        if($image->getWatermark()) {
            $logo = @imagecreatefrompng('logo/postLogo.png');
        }
        $background = @imagecreatefromjpeg('backgrounds/' . $image->getBackground());
        $destination_image = imagecreatetruecolor(imagesx($background), imagesy($background));

        if ($image->getColor() != '') {
            $rgb = $this->hex2rgb($image->getColor());
            $font_color = imagecolorallocate($destination_image, $rgb[0], $rgb[1], $rgb[2]);
        } else {
            $font_color = imagecolorallocate($destination_image, 0, 0, 0);

            foreach ($this->backgrounds as $item) {
                if (strtolower($item['filename']) == strtolower($image->getBackground())) {
                    if ($item['textcolor']) {
                        $rgb = $this->hex2rgb($item['textcolor']);
                        $font_color = imagecolorallocate($destination_image, $rgb[0], $rgb[1], $rgb[2]);
                    }
                    break;
                }
            }
        }
        $font = 'fonts/' . $image->getFont();
        $content = $image->getContent();
        $title = $image->getTitle();
        $author = $image->getAuthor();

        imagecopy($destination_image, $background, 0, 0, 0, 0, 940, 940);
        if($logo) {
            imagecopyresized($destination_image, $logo, 710, 870, 0, 0, imagesx($logo), imagesy($logo), imagesx($logo), imagesy($logo));
        }

        imagealphablending($destination_image, true);
        imagesavealpha($destination_image, true);
        ;

        $spacing = 1.5;
        $length = strlen($content);
        if ($length >= 572) {
            $fs = 29;
            $spacing = 1.2;
        } elseif ($length < 572 && $length >= 500) {
            $fs = 29;
        } elseif ($length < 500 && $length >= 450) {
            $fs = 29;
        } elseif ($length < 450 && $length >= 400) {
            $fs = 32;
        } elseif ($length < 400 && $length >= 350) {
            $fs = 36;
        } elseif ($length < 350) {
            $fs = 38;
        }

        //$textParams = $this->_getBlockTextParams($content, $fs, $font);

        $textParams = [
            'lineSpacing' => $fs * $spacing,
            'fontSize' => $fs,
        ];


        //imagettftext($destination_image, 15, 0, 600, 930, $font_color, 'fonts/Lato-Regular.ttf', round($textParams['fontSize'], 0) . '  ' . $font);
        $newText = $this->wrapText($content, $font, $textParams['fontSize']);

        
        $newText.=PHP_EOL . PHP_EOL . '- ' . $author;
        
        $titleWrapped= $this->wrapText($title, $font, $textParams['fontSize']);
        $newText.=PHP_EOL . $titleWrapped;

        $textBox = imageftbbox($textParams['fontSize'], 0, $font, $newText);
        $blocksize = [
            'width' => abs($textBox[0]) + abs($textBox[2]),
            'height' => abs($textBox[1]) + abs($textBox[5]),
        ];
        $lines = explode(PHP_EOL, $newText);
        $y = 450 - ((count($lines) ) * $textParams['lineSpacing'] / 2);

        //imagettftext($destination_image, $fs, 0, 30, $y, $font_color, $font, $newText);
        //line height
        $tmp = imageftbbox($textParams['fontSize'], 0, $font, 'dg');
        $textParams['lineSpacing'] = (abs($tmp[5]) + abs($tmp[1])) * $spacing;

        foreach ($lines as $line) {
            imagettftext($destination_image, $textParams['fontSize'], 0, 45, $y, $font_color, $font, $line);
            $y+=$textParams['lineSpacing'];
        }

        $image->setRendered($destination_image);
        //return $image;
        return $image->getImageContent();
    }

    private function wrapText($text, $font, $size) {
        $im = imagecreatetruecolor(900, 900);
        $xx = 0;
        $yy = $size;

        //space width
        $tmp = imageftbbox($size, 0, $font, ' ');
        $space = $tmp['width'] = abs($tmp[2]) + abs($tmp[0]);
        $words = explode(PHP_EOL, wordwrap($text, 2));

        $x = $xx;
        $y = $yy;
        $lineWidth = 0;

        $outText = "";
        foreach ($words as $word) {
            $part = imageftbbox($size, 0, $font, $word . ' ');
            $wordWidth = $part['width'] = abs($part[2]) + abs($tmp[0]);
            if ($x + $wordWidth > 935) {
                $outText.=PHP_EOL;
                $x = 0;
            }

            $outText.=$word . ' ';

            $x+=$wordWidth + $space;
        }
        return $outText;
    }


}
